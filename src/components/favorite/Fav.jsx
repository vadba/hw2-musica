import { Component } from 'react';
import s from './Fav.module.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';
import Cart from '../cart';

class Fav extends Component {
    render() {
        const { carts } = this.props;

        const renderFavs = fav => (
            <li key={fav.art} className={s.cart}>
                <img className={s.cartImg} src={fav.picUrl} alt='picture' />
                <div className={s.title}>{fav.title}</div>
                <p className={s.price}>{fav.price}</p>
            </li>
        );
        return carts.map(fav => renderFavs(fav));
    }
}

export default Fav;

Cart.protoType = {
  carts: PropTypes.object.isRequired,
};

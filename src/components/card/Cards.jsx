import { Component } from 'react';
import s from './Cards.module.scss';
import cn from 'classnames';
import { ReactComponent as StarIcon } from '../../assets/star.svg';
import { ReactComponent as StarIconOn } from '../../assets/starOn.svg';
import PropTypes from 'prop-types';

class Cards extends Component {
    render() {
        const { onClick, setFav, cards } = this.props;

        const favs = localStorage.getItem(2);

        const renderCards = card => (
            <li key={card.art} className={s.card}>
                <img className={s.cardImg} src={card.picUrl} alt='picture' />
                <div className={s.title}>{card.title}</div>
                <div className='stars' onClick={() => setFav(card)}>
                    {favs && localStorage.getItem(2).indexOf(card.art) !== -1 ? (
                        <>
                            <StarIconOn />
                            <StarIconOn />
                            <StarIconOn />
                            <StarIconOn />
                            <StarIconOn />
                            {console.log(localStorage.getItem(2).indexOf(card.art))}
                        </>
                    ) : (
                        <>
                            <StarIcon />
                            <StarIcon />
                            <StarIcon />
                            <StarIcon />
                            <StarIcon />
                        </>
                    )}
                </div>
                <div className={s.textCard}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Minus quo eligendi quam optio.</div>
                <div className={s.price_to_cart}>
                    <p className={s.price}>{card.price}</p>
                    <button className={s.toCart} onClick={() => onClick(card)}>
                        TO CART
                    </button>
                </div>
            </li>
        );

        return cards.map(card => renderCards(card));
    }
}

export default Cards;

Cards.protoType = {
    onClick: PropTypes.func.isRequired,
    setFav: PropTypes.func.isRequired,
    cards: PropTypes.object.isRequired,
};

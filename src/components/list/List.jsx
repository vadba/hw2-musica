import { Component } from 'react';
import s from './List.module.scss';
import cn from 'classnames';

class List extends Component {
    render() {
        const { children } = this.props;
        return <ul className={cn(s.ulist)}>{children}</ul>;
    }
}

export default List;

import { Component } from 'react';
import s from './Cart.module.scss';
import cn from 'classnames';
import PropTypes from 'prop-types';

class Cart extends Component {
    render() {
        const { carts } = this.props;

        const renderCards = cart => (
            <li key={cart.art} className={s.cart}>
                <img className={s.cartImg} src={cart.picUrl} alt='picture' />
                <div className={s.title}>{cart.title}</div>
                <p className={s.price}>{cart.price}</p>
            </li>
        );

        return carts.map(cart => renderCards(cart));
    }
}

export default Cart;

Cart.protoType = {
  carts: PropTypes.object.isRequired,
};

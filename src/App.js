import { Component } from 'react';
import logo from './logo.svg';
import './App.scss';
import Button from './ui/button/Button';
import Modal from './ui/modal/Modal';
import List from './components/list/List';
import Cards from './components/card/Cards';
import Cart from './components/cart/Cart';
import Fav from './components/favorite/Fav';
import { request } from './tools/request';
import { ReactComponent as CartIcon } from './assets/cart.svg';
import { ReactComponent as FavIcon } from './assets/fav.svg';

class App extends Component {
    state = {
        openedModal2: false,
        products: [],
        editModalData: {},
        carts: [],
        favs: [],
    };

    getProducts = async () => {
        const { res, err } = await request();
        if (res) {
            return this.setState({ products: res });
        }

        if (err) {
            return console.log(err);
        }
    };

    handleOpenModal2 = data => {
        this.setState({ openedModal2: true });
        this.setState({ editModalData: data });
    };

    handleCloseModal2 = () => {
        this.setState({ openedModal2: false });
    };

    componentDidMount() {
        localStorage.removeItem(1);
        localStorage.removeItem(2);
        this.getProducts();
    }

    setCardId = () => {
        if (localStorage.getItem(1) !== null) {
            if (!localStorage.getItem(1).indexOf(this.state.editModalData.art)) {
                return this.setState({ openedModal2: false });
            }
            this.setState({ cards: this.state.carts.push(this.state.editModalData) });
            const newStoreCart = localStorage.getItem(1) + ',' + this.state.editModalData.art;

            localStorage.setItem(1, newStoreCart);
        } else {
            this.setState({ cards: this.state.carts.push(this.state.editModalData) });
            localStorage.setItem(1, this.state.editModalData.art);
        }
        this.setState({ openedModal2: false });
    };

    setFav = data => {
        if (localStorage.getItem(2) !== null) {
            if (!localStorage.getItem(2).indexOf(data.art)) {
                return;
            }
            this.setState({ favs: [...this.state.favs, data] });
            const newFavs = localStorage.getItem(2) + ',' + data.art;

            localStorage.setItem(2, newFavs);
        } else {
            this.setState({ favs: [...this.state.favs, data] });
            localStorage.setItem(2, data.art);
        }
        console.log(this.state.favs);
    };

    render() {
        return (
            <div className='App'>
                <Modal opened={this.state.openedModal2} onClose={this.handleCloseModal2} header='TO Cart' closeButton={true} text='Lorem text' backgroundColor='yellow' backgroundColorTitle='green'>
                    <Button className={'buttonfirst'} text='OK' onClick={this.setCardId} backgroundColor='orange' />
                    <Button className={'buttonfirst'} text='Cancel' backgroundColor='orange' onClick={this.handleCloseModal2} />
                </Modal>
                <div className='App-conainer'>
                    <header className='App-header'>
                        <img src={logo} className='App-logo' alt='logo' />
                        <div className='header-reg-cart'>
                            <div className='link-register'>Login / Register</div>

                            <div className='hover-cart'>
                                {localStorage.getItem(1) ? <span className='int-cart'>{localStorage.getItem(1).split(',').length}</span> : ''}
                                <CartIcon />
                                <div className='cart-list'>
                                    <Cart carts={this.state.carts} />
                                </div>
                            </div>

                            <div className='hover-fav'>
                                {localStorage.getItem(2) ? <span className='int-fav'>{localStorage.getItem(2).split(',').length}</span> : ''}
                                <FavIcon />
                                <div className='fav-list'>
                                    {console.log(this.state.favs)}
                                    <Fav carts={this.state.favs} />
                                </div>
                            </div>
                        </div>
                    </header>
                    <main className='App-main'>
                        <List>
                            <Cards cards={this.state.products} onClick={this.handleOpenModal2} setFav={this.setFav} />
                        </List>
                    </main>
                </div>
            </div>
        );
    }
}

export default App;
